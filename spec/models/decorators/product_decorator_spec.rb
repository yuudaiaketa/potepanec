require 'rails_helper'

RSpec.describe 'decorators', type: :model do
  describe "関連商品を取得" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon_1]) }
    let!(:related_product_taxon_1) { create(:product, taxons: [taxon_1]) }
    let!(:other_product_taxon_2) { create(:product, taxons: [taxon_2]) }

    it "関連商品のみを表示" do
      expect(product.related_products).to match_array related_product_taxon_1
    end

    it "表示ページのidの商品が関連商品に表示されない" do
      expect(product.related_products).not_to match product
    end

    it "関連商品以外を表示しない" do
      expect(product.related_products).not_to match_array other_product_taxon_2
    end
  end
end
