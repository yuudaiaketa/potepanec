require 'rails_helper'
RSpec.feature "Potepans", type: :feature do
  let(:categories) { create(:categories) }
  let(:taxon)      { create(:taxon) }
  let!(:product)   { create(:product, taxons: [taxon]) }

  describe "商品カテゴリ一覧ページのテンプレート" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "商品リンクが正常に動作する" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    it "ヘッダーに商品名が表示される" do
      within ".page-title" do
        expect(page).to have_content taxon.name
      end
    end
  end
end
