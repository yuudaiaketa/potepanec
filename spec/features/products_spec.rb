require 'rails_helper'

RSpec.feature "Potepans", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  describe '商品詳細ページのテンプレート' do
    before do
      visit potepan_product_path(product.id)
    end

    it "対象商品のデータが表示される" do
      within(".media-body") do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    it "関連商品が表示される" do
      within(".productCaption") do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end

    it 'Homeリンクが正常に動作する' do
      click_link "Home", match: :prefer_exact
      expect(current_path).to eq potepan_path
    end

    it '一覧ページへ戻るリンクが正常に動作する' do
      click_link "一覧ページへ戻る", match: :prefer_exact
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '関連商品をクリックして商品ページへ遷移できるか' do
      click_link "#{related_product.name}", match: :first
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it '関連商品が複数あるケース' do
      expect(related_products.count).to eq 4
    end
  end
end
