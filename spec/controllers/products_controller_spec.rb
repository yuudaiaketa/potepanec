require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  include ApplicationHelper
  describe "商品詳細ページへのアクセス" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon_1]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon_1]) }
    let(:other_product) { create(:product, taxons: [taxon_2]) }

    before do
      get :show, params: { id: product.id }
    end

    it "200 OKを返す" do
      expect(response).to have_http_status(:ok)
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@productが期待される値を持つ" do
      expect(assigns(:product)).to eq product
    end

    it "@related_productsの期待される値が4つ" do
      expect(assigns(:related_products).length).to eq 4
    end

    it "@related_productsが異なる商品を含まない" do
      expect(assigns(:related_products)).not_to match other_product
    end
  end
end
