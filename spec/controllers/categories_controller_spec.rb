require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '商品一覧ページへのアクセス' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
    let(:product)  { create(:product, taxons: [taxon]) }

    before { get :show, params: { id: taxon.id } }

    it '200 OKを返す' do
      expect(response).to have_http_status(:ok)
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it '@categoriesが期待している値を持つ' do
      expect(assigns(:categories)).to contain_exactly(taxonomy)
    end

    it '@taxonが期待している値を持つ' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@productsが期待している値を持つ' do
      expect(assigns(:products)).to eq taxon.all_products
    end
  end
end
