require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  let(:taxon) { create(:taxon) }

  describe "タイトルの表示" do
    it 'トップページのタイトルの表示' do
      expect(full_title('')).to match('BIGBAG Store')
    end

    it "カテゴリーページのタイトルが表示される" do
      expect(full_title(taxon.name)).to match "#{taxon.name} - BIGBAG Store"
    end
  end
end
