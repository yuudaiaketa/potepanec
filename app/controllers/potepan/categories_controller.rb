class Potepan::CategoriesController < ApplicationController
  def show
    @categories = Spree::Taxonomy.includes([:root])
    @taxon      = Spree::Taxon.find(params[:id])
    @products   = @taxon.all_products.includes(master: [:default_price, :images])
  end
end
