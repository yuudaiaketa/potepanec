Spree::Image.attachment_definitions[:attachment][:styles] = {
  mini: '128x128>',
  small: '256x256>',
  product: '512x512>',
  large: '1024x1024>'
}
